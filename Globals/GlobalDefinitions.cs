﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EKU.Globals
{
    #region Results types

    public struct Result
    {
        public int resultCode;
        public string resultMessage;
    }

    public struct ResultAdd
    {
        public int resultCode;
        public string resultMessage;
        public int NewEntityID;
    }

    public struct ResultAddTX
    {
        public int resultCode;
        public string resultMesssage;
        public long NewEntityID;
    }

    public struct ResultDetail
    {
        public int resultCode;
        public string summaryResultMesssage;
        public List<string> detailResultMesssage;
    }

    public struct ResultExistanceCheck
    {
        public int existenceCheckresultCode;
        public string existenceCheckResultMessage;
        public bool DoesExist;
    }

    #endregion

    #region CodeExecution

    public class CodeExecution
    {
        static public ResultAdd InsertSuccessful(int newEntityId, string message = null)
        {
            return new ResultAdd
            {
                NewEntityID = newEntityId,
                resultCode = 0,
                resultMessage = string.IsNullOrEmpty(message) ? "Added successfully" : message,
            };
        }

        static public ResultAdd InsertFailed(string message = "")
        {
            return new ResultAdd
            {
                NewEntityID = 0,
                resultCode = 1,
                resultMessage = message,
            };
        }

        static public Result Succeeded(string message = "")
        {
            return new Result
            {
                resultCode = 0,
                resultMessage = message,
            };
        }

        static public Result NotFound(string message = "")
        {
            return new Result
            {
                resultCode = 1,
                resultMessage = message,
            };
        }

        static public Result Failed(string message = "")
        {
            return new Result
            {
                resultCode = 2,
                resultMessage = message,
            };
        }

        static public Result Duplicate(string message = "")
        {
            return new Result
            {
                resultCode = 3,
                resultMessage = message,
            };
        }


        static public ResultExistanceCheck Exists(string message = "")
        {
            return new ResultExistanceCheck
            {
                existenceCheckresultCode = 0,
                existenceCheckResultMessage = message,
                DoesExist = true,
            };
        }

        static public ResultExistanceCheck DoesNotExists(string message = "")
        {
            return new ResultExistanceCheck
            {
                existenceCheckresultCode = 0,
                existenceCheckResultMessage = message,
                DoesExist = false,
            };
        }

        static public ResultExistanceCheck FailedExistance(string message = "")
        {
            return new ResultExistanceCheck
            {
                existenceCheckresultCode = 1,
                existenceCheckResultMessage = message,
                DoesExist = false,
            };
        }
    }

    #endregion

    #region Common

    public static class Common
    {

        public static readonly string EKU_ROOT_DIRECTORY = @"C:\EKU";

        public static readonly string EKU_TEMPLATE_DIRECTORY = @"Resources\Mobiz Template.xlsx";


        public static readonly string FILE_HEADER = "00";

        public static readonly string BALANCE_BROUGHT_FORWARD = "00BALANCE BROUGHT FORWARD";

        public static readonly string TOTAL = "00TOTAL";

        public static readonly string PAYMENT_MADE = "00PAYMENT";

        public static string GenerateEKUFileName(string filePath)
        {
            string fileName = Path.GetFileName(filePath);
            string cycleNumber = "";

            if (fileName.Contains(" "))
            {
                cycleNumber = fileName.Split(' ')[1];
            }

            return "MobizGenerated" + "_" + DateTime.Now.ToString("yyyyMMdd") + "_" + cycleNumber + ".xlsx";
        }
    }

    #endregion
}
