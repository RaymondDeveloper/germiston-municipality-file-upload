﻿using DevExpress.LookAndFeel;
using EKU.GEW;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EKU
{
    public partial class Main : Form
    {
        public Main()
        {
            DevExpress.UserSkins.BonusSkins.Register();
            Application.EnableVisualStyles();
            InitializeComponent();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormGEWUpload form = new FormGEWUpload();
            form.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DevExpress.UserSkins.BonusSkins.Register();
            Application.EnableVisualStyles();
            UserLookAndFeel.Default.SetSkinStyle("Office 2019 Colorful");
        }

        private void barButtonItemViewUploads_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormViewUploads form = new FormViewUploads();
            form.MdiParent = this;
            form.Show();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormViewAccounts form = new FormViewAccounts();
            form.MdiParent = this;
            form.Show();
        }
    }
}
