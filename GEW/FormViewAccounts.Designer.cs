﻿namespace EKU.GEW
{
    partial class FormViewAccounts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridViewImports = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colpkAccountID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colfkImportID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccountNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaymentUntil = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceForward = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCurrentLevy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountPayable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaymentsMade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCellNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRunDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEditFilterByPeriod = new DevExpress.XtraEditors.CheckEdit();
            this.DateEditPeriodStartDate = new DevExpress.XtraEditors.DateEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.DateEditPeriodEndDate = new DevExpress.XtraEditors.DateEdit();
            this.TableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.eKUDataSet = new EKU.DAL.EKUDataSet();
            this.virtualAccountsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.virtualAccountsTableAdapter = new EKU.DAL.EKUDataSetTableAdapters.virtualAccountsTableAdapter();
            this.TableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewImports)).BeginInit();
            this.TableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditFilterByPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodEndDate.Properties)).BeginInit();
            this.TableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eKUDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.virtualAccountsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.ColumnCount = 1;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.Controls.Add(this.GridControl1, 0, 1);
            this.TableLayoutPanel1.Controls.Add(this.TableLayoutPanel2, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.TableLayoutPanel3, 0, 2);
            this.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 3;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(921, 524);
            this.TableLayoutPanel1.TabIndex = 2;
            // 
            // GridControl1
            // 
            this.GridControl1.DataSource = this.virtualAccountsBindingSource;
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridControl1.Location = new System.Drawing.Point(3, 35);
            this.GridControl1.MainView = this.GridViewImports;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(915, 451);
            this.GridControl1.TabIndex = 3;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridViewImports});
            // 
            // GridViewImports
            // 
            this.GridViewImports.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.GridViewImports.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridViewImports.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GridViewImports.ColumnPanelRowHeight = 40;
            this.GridViewImports.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colpkAccountID,
            this.colfkImportID,
            this.colAccountNo,
            this.colName,
            this.colPaymentUntil,
            this.colBalanceForward,
            this.colTotalCurrentLevy,
            this.colAmountPayable,
            this.colPaymentsMade,
            this.colDueDate,
            this.colCellNo,
            this.colRunDate,
            this.colFileName});
            this.GridViewImports.GridControl = this.GridControl1;
            this.GridViewImports.Name = "GridViewImports";
            this.GridViewImports.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GridViewImports.OptionsSelection.MultiSelect = true;
            this.GridViewImports.OptionsView.ShowAutoFilterRow = true;
            this.GridViewImports.OptionsView.ShowDetailButtons = false;
            this.GridViewImports.OptionsView.ShowGroupPanel = false;
            this.GridViewImports.OptionsView.ShowIndicator = false;
            // 
            // colpkAccountID
            // 
            this.colpkAccountID.FieldName = "pkAccountID";
            this.colpkAccountID.Name = "colpkAccountID";
            // 
            // colfkImportID
            // 
            this.colfkImportID.FieldName = "fkImportID";
            this.colfkImportID.Name = "colfkImportID";
            // 
            // colAccountNo
            // 
            this.colAccountNo.FieldName = "AccountNo";
            this.colAccountNo.Name = "colAccountNo";
            this.colAccountNo.OptionsColumn.AllowEdit = false;
            this.colAccountNo.OptionsColumn.ReadOnly = true;
            this.colAccountNo.Visible = true;
            this.colAccountNo.VisibleIndex = 0;
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            // 
            // colPaymentUntil
            // 
            this.colPaymentUntil.FieldName = "PaymentUntil";
            this.colPaymentUntil.Name = "colPaymentUntil";
            this.colPaymentUntil.OptionsColumn.AllowEdit = false;
            this.colPaymentUntil.OptionsColumn.ReadOnly = true;
            this.colPaymentUntil.Visible = true;
            this.colPaymentUntil.VisibleIndex = 2;
            // 
            // colBalanceForward
            // 
            this.colBalanceForward.FieldName = "BalanceForward";
            this.colBalanceForward.Name = "colBalanceForward";
            this.colBalanceForward.OptionsColumn.AllowEdit = false;
            this.colBalanceForward.OptionsColumn.ReadOnly = true;
            this.colBalanceForward.Visible = true;
            this.colBalanceForward.VisibleIndex = 3;
            // 
            // colTotalCurrentLevy
            // 
            this.colTotalCurrentLevy.FieldName = "TotalCurrentLevy";
            this.colTotalCurrentLevy.Name = "colTotalCurrentLevy";
            this.colTotalCurrentLevy.OptionsColumn.AllowEdit = false;
            this.colTotalCurrentLevy.OptionsColumn.ReadOnly = true;
            this.colTotalCurrentLevy.Visible = true;
            this.colTotalCurrentLevy.VisibleIndex = 4;
            // 
            // colAmountPayable
            // 
            this.colAmountPayable.FieldName = "AmountPayable";
            this.colAmountPayable.Name = "colAmountPayable";
            this.colAmountPayable.OptionsColumn.AllowEdit = false;
            this.colAmountPayable.OptionsColumn.ReadOnly = true;
            this.colAmountPayable.Visible = true;
            this.colAmountPayable.VisibleIndex = 5;
            // 
            // colPaymentsMade
            // 
            this.colPaymentsMade.FieldName = "PaymentsMade";
            this.colPaymentsMade.Name = "colPaymentsMade";
            this.colPaymentsMade.OptionsColumn.AllowEdit = false;
            this.colPaymentsMade.OptionsColumn.ReadOnly = true;
            this.colPaymentsMade.Visible = true;
            this.colPaymentsMade.VisibleIndex = 6;
            // 
            // colDueDate
            // 
            this.colDueDate.FieldName = "DueDate";
            this.colDueDate.Name = "colDueDate";
            this.colDueDate.OptionsColumn.AllowEdit = false;
            this.colDueDate.OptionsColumn.ReadOnly = true;
            this.colDueDate.Visible = true;
            this.colDueDate.VisibleIndex = 7;
            // 
            // colCellNo
            // 
            this.colCellNo.FieldName = "CellNo";
            this.colCellNo.Name = "colCellNo";
            this.colCellNo.OptionsColumn.AllowEdit = false;
            this.colCellNo.OptionsColumn.ReadOnly = true;
            this.colCellNo.Visible = true;
            this.colCellNo.VisibleIndex = 8;
            // 
            // colRunDate
            // 
            this.colRunDate.FieldName = "RunDate";
            this.colRunDate.Name = "colRunDate";
            this.colRunDate.OptionsColumn.AllowEdit = false;
            this.colRunDate.OptionsColumn.ReadOnly = true;
            this.colRunDate.Visible = true;
            this.colRunDate.VisibleIndex = 9;
            // 
            // colFileName
            // 
            this.colFileName.FieldName = "FileName";
            this.colFileName.Name = "colFileName";
            this.colFileName.OptionsColumn.AllowEdit = false;
            this.colFileName.OptionsColumn.ReadOnly = true;
            this.colFileName.Visible = true;
            this.colFileName.VisibleIndex = 10;
            // 
            // TableLayoutPanel2
            // 
            this.TableLayoutPanel2.AutoSize = true;
            this.TableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TableLayoutPanel2.ColumnCount = 6;
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel2.Controls.Add(this.btnRefresh, 5, 0);
            this.TableLayoutPanel2.Controls.Add(this.CheckEditFilterByPeriod, 1, 0);
            this.TableLayoutPanel2.Controls.Add(this.DateEditPeriodStartDate, 2, 0);
            this.TableLayoutPanel2.Controls.Add(this.LabelControl1, 3, 0);
            this.TableLayoutPanel2.Controls.Add(this.DateEditPeriodEndDate, 4, 0);
            this.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.TableLayoutPanel2.Name = "TableLayoutPanel2";
            this.TableLayoutPanel2.RowCount = 1;
            this.TableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel2.Size = new System.Drawing.Size(915, 26);
            this.TableLayoutPanel2.TabIndex = 0;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(837, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 20);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // CheckEditFilterByPeriod
            // 
            this.CheckEditFilterByPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CheckEditFilterByPeriod.EditValue = true;
            this.CheckEditFilterByPeriod.Location = new System.Drawing.Point(459, 3);
            this.CheckEditFilterByPeriod.Name = "CheckEditFilterByPeriod";
            this.CheckEditFilterByPeriod.Properties.Caption = "Filter by run date";
            this.CheckEditFilterByPeriod.Size = new System.Drawing.Size(144, 20);
            this.CheckEditFilterByPeriod.TabIndex = 0;
            // 
            // DateEditPeriodStartDate
            // 
            this.DateEditPeriodStartDate.EditValue = null;
            this.DateEditPeriodStartDate.Location = new System.Drawing.Point(609, 3);
            this.DateEditPeriodStartDate.Name = "DateEditPeriodStartDate";
            this.DateEditPeriodStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEditPeriodStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEditPeriodStartDate.Size = new System.Drawing.Size(100, 20);
            this.DateEditPeriodStartDate.TabIndex = 1;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelControl1.Location = new System.Drawing.Point(715, 6);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(10, 13);
            this.LabelControl1.TabIndex = 3;
            this.LabelControl1.Text = "to";
            // 
            // DateEditPeriodEndDate
            // 
            this.DateEditPeriodEndDate.EditValue = null;
            this.DateEditPeriodEndDate.Location = new System.Drawing.Point(731, 3);
            this.DateEditPeriodEndDate.Name = "DateEditPeriodEndDate";
            this.DateEditPeriodEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEditPeriodEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEditPeriodEndDate.Size = new System.Drawing.Size(100, 20);
            this.DateEditPeriodEndDate.TabIndex = 2;
            // 
            // TableLayoutPanel3
            // 
            this.TableLayoutPanel3.AutoSize = true;
            this.TableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TableLayoutPanel3.ColumnCount = 7;
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.Controls.Add(this.btnDelete, 5, 0);
            this.TableLayoutPanel3.Controls.Add(this.btnClose, 6, 0);
            this.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel3.Location = new System.Drawing.Point(3, 492);
            this.TableLayoutPanel3.Name = "TableLayoutPanel3";
            this.TableLayoutPanel3.RowCount = 1;
            this.TableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel3.Size = new System.Drawing.Size(915, 29);
            this.TableLayoutPanel3.TabIndex = 1;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(746, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 23);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Export to excel";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(837, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // eKUDataSet
            // 
            this.eKUDataSet.DataSetName = "EKUDataSet";
            this.eKUDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // virtualAccountsBindingSource
            // 
            this.virtualAccountsBindingSource.DataMember = "virtualAccounts";
            this.virtualAccountsBindingSource.DataSource = this.eKUDataSet;
            // 
            // virtualAccountsTableAdapter
            // 
            this.virtualAccountsTableAdapter.ClearBeforeFill = true;
            // 
            // FormViewAccounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 524);
            this.Controls.Add(this.TableLayoutPanel1);
            this.Name = "FormViewAccounts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Accounts";
            this.Load += new System.EventHandler(this.FormViewAccounts_Load);
            this.TableLayoutPanel1.ResumeLayout(false);
            this.TableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewImports)).EndInit();
            this.TableLayoutPanel2.ResumeLayout(false);
            this.TableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditFilterByPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodEndDate.Properties)).EndInit();
            this.TableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.eKUDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.virtualAccountsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridViewImports;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel2;
        internal DevExpress.XtraEditors.SimpleButton btnRefresh;
        internal DevExpress.XtraEditors.CheckEdit CheckEditFilterByPeriod;
        internal DevExpress.XtraEditors.DateEdit DateEditPeriodStartDate;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.DateEdit DateEditPeriodEndDate;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel3;
        internal DevExpress.XtraEditors.SimpleButton btnDelete;
        internal DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraGrid.Columns.GridColumn colpkAccountID;
        private DevExpress.XtraGrid.Columns.GridColumn colfkImportID;
        private DevExpress.XtraGrid.Columns.GridColumn colAccountNo;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaymentUntil;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceForward;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCurrentLevy;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountPayable;
        private DevExpress.XtraGrid.Columns.GridColumn colPaymentsMade;
        private DevExpress.XtraGrid.Columns.GridColumn colDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCellNo;
        private DevExpress.XtraGrid.Columns.GridColumn colRunDate;
        private DevExpress.XtraGrid.Columns.GridColumn colFileName;
        private DAL.EKUDataSet eKUDataSet;
        private System.Windows.Forms.BindingSource virtualAccountsBindingSource;
        private DAL.EKUDataSetTableAdapters.virtualAccountsTableAdapter virtualAccountsTableAdapter;
    }
}