﻿using System;
using System.Collections.Generic;
using System.Linq;
using EKU.Globals;
using Microsoft.VisualBasic.FileIO;
using System.IO;
using OfficeOpenXml;
using System.Globalization;
using EKU.DAL;

namespace EKU.GEW
{
    public class UploadGEW
    {

        private List<RawData> _headerInfo; // Name, PayableAmount
        private List<RawData> _detailsInfo; // BalanceBroughtFoward
        private List<RawData> _totalsInfo;
        private List<RawData> _paymentInfo;
        private List<ParsedData> _parsedList;

        private List<string> _UploadFileFormatLines;

        private string _filePath;

        private bool _updateDatabase;

        #region Public properties

        public UploadGEW(string filePath, bool updateDatabase)
        {
            _filePath = filePath;
            _updateDatabase = updateDatabase;

            _headerInfo = new List<RawData>();
            _detailsInfo = new List<RawData>();
            _totalsInfo = new List<RawData>();
            _paymentInfo = new List<RawData>();

            _parsedList = new List<ParsedData>();
        }

        public string FileFormat
        {
            get
            {
                string combinedstring = "";

                foreach (var fline in _UploadFileFormatLines)
                    combinedstring = string.Concat(combinedstring, Environment.NewLine, fline);

                return combinedstring;
            }
        }

        #endregion

        #region Internal structure

        private class RawData
        {
            public string AccountNo;
            public string Name;
            public string PaymentUntil;
            public string BalanceForward;
            public string TotalCurrentLevy;
            public string AmountPayable;
            public string PaymentsMade;
            public string DueDate;
            public string CellNo;
            public string PaymentDate;
        }

        private class ParsedData
        {
            public string AccountNo;
            public string Name;
            public DateTime PaymentUntil;
            public double BalanceForward;
            public double TotalCurrentLevy;
            public double AmountPayable;
            public double PaymentsMade;
            public DateTime DueDate;
            public string CellNo;
            public string Destination;
            public DateTime? PaymentDate;
        }

        private class Import
        {
            public string FileName;
            public DateTime EffectiveDate;
            public DateTime RunDate;
            public int NoOfRecords;
            public string FilePath;
            public bool ImportSuccessful;
        }

        #endregion

        #region Internal methods

        private Result ReadRawFlatFile()
        {
            List<ParsedData> parsedList = new List<ParsedData>();

            string[] allLines = File.ReadAllLines(_filePath);

            int lineCount = 0;
            string header = string.Empty;

            foreach (string line in allLines)
            {
                lineCount++;
                if (lineCount == 1 || string.IsNullOrEmpty(line))
                    continue;

                try
                {
                    header = line.Substring(59, 2);

                    if (header == Common.FILE_HEADER)
                    {
                        RawData rawData = new RawData();
                        rawData.AccountNo = line.Substring(10, 10).Trim();
                        rawData.Name = line.Substring(61, 35).Trim();
                        rawData.AmountPayable = line.Substring(577, 8).Trim();
                        rawData.DueDate = line.Substring(564, 10).Trim();
                        rawData.PaymentUntil = line.Substring(31, 10).Trim();
                        rawData.CellNo = "0"; // TODO: where do i find?
                        _headerInfo.Add(rawData);
                    }

                    else if (line.Contains(Common.BALANCE_BROUGHT_FORWARD))
                    {
                        RawData rawData = new RawData();
                        rawData.AccountNo = line.Substring(10, 10).Trim();
                        rawData.BalanceForward = line.Substring(119, 16).Trim();
                        _detailsInfo.Add(rawData);
                    }

                    else if (line.Contains(Common.PAYMENT_MADE))
                    {
                        RawData rawData = new RawData();
                        rawData.AccountNo = line.Substring(10, 10).Trim();
                        rawData.PaymentsMade = line.Substring(120, 10).Trim();
                        rawData.PaymentDate = line.Substring(59, 8);
                        _paymentInfo.Add(rawData);
                    }

                    else if (line.Contains(Common.TOTAL))
                    {
                        RawData rawData = new RawData();
                        rawData.AccountNo = line.Substring(10, 10).Trim();
                        rawData.TotalCurrentLevy = line.Substring(90, 15).Trim();
                        _totalsInfo.Add(rawData);
                    }
                }
                catch (Exception ex)
                {
                    return CodeExecution.Failed(string.Format("Failed to read raw data. Error on line: {0} - Error: {1}", lineCount.ToString(), ex.Message));
                }
            }

            return CodeExecution.Succeeded("Successfully read raw data");
        }

        private Result ParseData()
        {
            try
            {
                if (_headerInfo.Any())
                {
                    foreach (var a in _headerInfo)
                    {
                        double balanceForward = 0;
                        double totalCurrentLevy = 0;
                        double paymentsMade = 0;
                        double amountPayable = Convert.ToDouble(a.AmountPayable, CultureInfo.InvariantCulture);

                        DateTime? paymentDate = null;

                        if (_detailsInfo.Any(p => p.AccountNo == a.AccountNo))
                        {
                            balanceForward = Convert.ToDouble(_detailsInfo.Where(p => p.AccountNo == a.AccountNo).First().BalanceForward,
                                CultureInfo.InvariantCulture);
                        }           

                        if (_totalsInfo.Any(p => p.AccountNo == a.AccountNo))
                        {
                            totalCurrentLevy = Convert.ToDouble(_totalsInfo.Where(p => p.AccountNo == a.AccountNo).First().TotalCurrentLevy, CultureInfo.InvariantCulture);
                        }

                        if (_paymentInfo.Any(p => p.AccountNo == a.AccountNo))
                        {
                            paymentsMade = _paymentInfo.Where(p => p.AccountNo == a.AccountNo).Sum(x => Convert.ToDouble(x.PaymentsMade, CultureInfo.InvariantCulture));
                            paymentDate = DateTime.Parse(_paymentInfo.Where(p => p.AccountNo == a.AccountNo).OrderByDescending(p => p.PaymentDate).First().PaymentDate);
                        }

                        _parsedList.Add(new ParsedData
                        {
                            AccountNo = a.AccountNo,
                            Name = a.Name,
                            PaymentUntil = DateTime.Parse(a.PaymentUntil),
                            BalanceForward = balanceForward,
                            PaymentsMade = paymentsMade,
                            TotalCurrentLevy = totalCurrentLevy,
                            AmountPayable = amountPayable,
                            DueDate = DateTime.Parse(a.DueDate),
                            PaymentDate = paymentDate,
                            CellNo = a.CellNo,
                            Destination = "Designated storage area, accessable via Web Service",
                        });
                    }
                }

                return CodeExecution.Succeeded("Successfully parsed data");
            }
            catch (Exception ex)
            {
                return CodeExecution.Failed(string.Concat("Failed to parse data: ", ex.Message));
            }
        }

        private Result WriteToSpreadsheet()
        {
            try
            {
                // Create a new file directory to save the generated file.
                bool directoryExists = Directory.Exists(Common.EKU_ROOT_DIRECTORY);
                if (!directoryExists)
                    Directory.CreateDirectory(Common.EKU_ROOT_DIRECTORY);

                FileInfo templateFile = new FileInfo(Common.EKU_TEMPLATE_DIRECTORY);
                FileInfo newFile = new FileInfo(Path.Combine(Common.EKU_ROOT_DIRECTORY, Common.GenerateEKUFileName(_filePath)));

                using (var package = new ExcelPackage(newFile, templateFile))
                {
                    ExcelWorkbook workBook = package.Workbook;
                    ExcelWorksheet worksheet = workBook.Worksheets["Sheet1"];

                    // first row is headers
                    int startRow = 2;
                    foreach (var rec in _parsedList)
                    {
                        worksheet.Cells[startRow, 1].Value = rec.AccountNo;
                        worksheet.Cells[startRow, 2].Value = rec.PaymentUntil;
                        worksheet.Cells[startRow, 3].Value = rec.Name;
                        worksheet.Cells[startRow, 4].Value = rec.BalanceForward;
                        worksheet.Cells[startRow, 5].Value = rec.PaymentsMade; // todo
                        worksheet.Cells[startRow, 6].Value = rec.PaymentDate; // todo
                        worksheet.Cells[startRow, 7].Value = rec.TotalCurrentLevy;
                        worksheet.Cells[startRow, 8].Value = rec.AmountPayable;
                        worksheet.Cells[startRow, 9].Value = rec.DueDate;
                        worksheet.Cells[startRow, 10].Value = 0; //todo ;
                        worksheet.Cells[startRow, 11].Value = rec.Destination;

                        startRow++;
                    }

                    package.Save();
                }

                return CodeExecution.Succeeded("Successfully generated Mobiz file");
            }
            catch (Exception ex)
            {
                return CodeExecution.Failed(ex.Message);
            }
        }

        private Result SaveToDB()
        {
            try
            {
                string filename = Path.GetFileName(_filePath);

                DAL.EKUDataSetTableAdapters.ImportsTableAdapter taImports = new DAL.EKUDataSetTableAdapters.ImportsTableAdapter();
                DAL.EKUDataSetTableAdapters.AccountsTableAdapter taAccounts = new DAL.EKUDataSetTableAdapters.AccountsTableAdapter();

                EKUDataSet.ImportsDataTable dtImport = taImports.GetDataByFileName(filename);

                if (dtImport.Count() > 0)
                {
                    return CodeExecution.Succeeded(string.Format("File '{0}' has already been uploaded. However, Mobiz file has been generated successfully", filename));
                }

                else
                {
                    // Create a new import
                    Import i = new Import
                    {
                        FileName = filename,
                        EffectiveDate = DateTime.Now, // TODO: Ask Gerhard how to get effective date from file.
                        RunDate = DateTime.Now,
                        NoOfRecords = _parsedList.Count(),
                        FilePath = "N/A",
                        ImportSuccessful = true
                    };

                    int newImportID = Convert.ToInt32(taImports.InsertImport(i.RunDate, i.FileName, i.EffectiveDate, i.NoOfRecords, i.FilePath, i.ImportSuccessful));

                    // Save accounts
                    if (_parsedList.Any())
                    {
                        foreach (var rec in _parsedList)
                        {
                            taAccounts.InsertAccount(newImportID, rec.AccountNo, rec.Name, rec.PaymentUntil, rec.BalanceForward, rec.TotalCurrentLevy, rec.AmountPayable, rec.PaymentsMade, rec.DueDate, rec.CellNo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return CodeExecution.Failed(string.Format("Failed to save data to DB: '{0}'", ex.Message));
            }

            return CodeExecution.Succeeded("Successfully uploaded data to the database!");
        }

        #endregion

        #region Public properties

        public UploadGEW()
        {
            _UploadFileFormatLines = new List<string>();

            _UploadFileFormatLines.Add("Germiston Municipality file upload");

            _UploadFileFormatLines.Add(Environment.NewLine);
            _UploadFileFormatLines.Add("To upload file");
            _UploadFileFormatLines.Add(" - Select the file to upload and click save");

            _UploadFileFormatLines.Add(Environment.NewLine);
            _UploadFileFormatLines.Add("Generated file are saved to:");
            _UploadFileFormatLines.Add(" - C:\\EKU\\");
            _UploadFileFormatLines.Add(" - C:\\EKU\\ - This folder will be created if it does not exists");

            _UploadFileFormatLines.Add(Environment.NewLine);
            _UploadFileFormatLines.Add("General:");
            _UploadFileFormatLines.Add(" - One of the initiatives is to create electronic statements for distribution to their complete customer base");
            _UploadFileFormatLines.Add(" - They operate on cycles , which are 17 in total and these cycles represent certain areas within Germiston");
            _UploadFileFormatLines.Add(" - The statement information is sent to us via e-mail in a flat file format");
            _UploadFileFormatLines.Add(" - We are making use of a solution called Mobiz.  Their technology allows us to create a Statement (mini online statement.");
            _UploadFileFormatLines.Add(" - The upload allow us to read the flat file and extract specific information");
            _UploadFileFormatLines.Add(" - The completed statement should then be converted into a PDF document and saved to a designated directory");
            _UploadFileFormatLines.Add(" - The PDF file name should reference the Customer’s Account number and Statement date. (eg. 2604246270 20211006).");
            _UploadFileFormatLines.Add(" - To create the mini-statement the program will have to extract some information from the same flat file.");
            _UploadFileFormatLines.Add(" - The following fields will be populated into the Excel template for Mobiz upload.");

            _UploadFileFormatLines.Add(Environment.NewLine);
            _UploadFileFormatLines.Add(" - Field names:");
            _UploadFileFormatLines.Add(" - Account_no");
            _UploadFileFormatLines.Add(" - Name");
            _UploadFileFormatLines.Add(" - Payment_until");
            _UploadFileFormatLines.Add(" - Balance_forward");
            _UploadFileFormatLines.Add(" - Total_current_levy");
            _UploadFileFormatLines.Add(" - Amount_payable");
            _UploadFileFormatLines.Add(" - Cell_no");
        }

        public void Clear()
        {
            _headerInfo.Clear();
            _detailsInfo.Clear();
            _totalsInfo.Clear();
            _paymentInfo.Clear();
            _parsedList.Clear();
        }

        public Result Upload()
        {
            // 1) Clear all data
            Clear();

            // 2) Read the Flat file
            Result results = ReadRawFlatFile();

            if (results.resultCode == 0)
            {
                // 3) Parse data read from Flat file
                results = ParseData();

                if (results.resultCode == 0)
                {
                    // 4) Populate an excel template for Mobiz
                    results = WriteToSpreadsheet();

                    if (results.resultCode == 0)
                    {
                        if (_updateDatabase == true)
                        {
                            // 5) Persits extracted data to the database
                            results = SaveToDB();
                        }
                    }
                }
            }

            return results;
        }

        #endregion
    }
}
