﻿namespace EKU.GEW
{
    partial class progressWindowUpload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.uploadProcessor = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.uploadProcessor.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(17, 16);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(80, 13);
            this.LabelControl1.TabIndex = 7;
            this.LabelControl1.Text = "Processing file...";
            // 
            // uploadProcessor
            // 
            this.uploadProcessor.EditValue = 0;
            this.uploadProcessor.Location = new System.Drawing.Point(16, 35);
            this.uploadProcessor.Name = "uploadProcessor";
            this.uploadProcessor.Size = new System.Drawing.Size(328, 18);
            this.uploadProcessor.TabIndex = 6;
            // 
            // progressWindowUpload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 69);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.uploadProcessor);
            this.Name = "progressWindowUpload";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Uploading";
            this.Load += new System.EventHandler(this.progressWindowUpload_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uploadProcessor.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.MarqueeProgressBarControl uploadProcessor;
    }
}