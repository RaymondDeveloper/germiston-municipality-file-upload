﻿namespace EKU.GEW
{
    partial class FormGEWUpload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtFileUploadPath = new DevExpress.XtraEditors.TextEdit();
            this.btnSetFilePath = new DevExpress.XtraEditors.SimpleButton();
            this.btnUploadFile = new DevExpress.XtraEditors.SimpleButton();
            this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.TableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.XtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.XtraTabPageGeneralInfo = new DevExpress.XtraTab.XtraTabPage();
            this.MemoEditFileFormat = new DevExpress.XtraEditors.MemoEdit();
            this.TableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEditUpdateDatabase = new DevExpress.XtraEditors.CheckEdit();
            this.TableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).BeginInit();
            this.GroupControl1.SuspendLayout();
            this.TableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFileUploadPath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).BeginInit();
            this.GroupControl2.SuspendLayout();
            this.TableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XtraTabControl1)).BeginInit();
            this.XtraTabControl1.SuspendLayout();
            this.XtraTabPageGeneralInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MemoEditFileFormat.Properties)).BeginInit();
            this.TableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditUpdateDatabase.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // TableLayoutPanel2
            // 
            this.TableLayoutPanel2.ColumnCount = 1;
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel2.Controls.Add(this.GroupControl1, 0, 0);
            this.TableLayoutPanel2.Controls.Add(this.GroupControl2, 0, 1);
            this.TableLayoutPanel2.Controls.Add(this.TableLayoutPanel3, 0, 2);
            this.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel2.Name = "TableLayoutPanel2";
            this.TableLayoutPanel2.RowCount = 3;
            this.TableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel2.Size = new System.Drawing.Size(646, 495);
            this.TableLayoutPanel2.TabIndex = 3;
            // 
            // GroupControl1
            // 
            this.GroupControl1.AutoSize = true;
            this.GroupControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GroupControl1.Controls.Add(this.TableLayoutPanel1);
            this.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.GroupControl1.Location = new System.Drawing.Point(3, 3);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(640, 51);
            this.GroupControl1.TabIndex = 0;
            this.GroupControl1.Text = "File upload settings";
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.AutoSize = true;
            this.TableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TableLayoutPanel1.ColumnCount = 4;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel1.Controls.Add(this.LabelControl1, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.txtFileUploadPath, 1, 0);
            this.TableLayoutPanel1.Controls.Add(this.btnSetFilePath, 2, 0);
            this.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TableLayoutPanel1.Location = new System.Drawing.Point(2, 23);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 3;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel1.Size = new System.Drawing.Size(636, 26);
            this.TableLayoutPanel1.TabIndex = 0;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelControl1.Location = new System.Drawing.Point(3, 6);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(64, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "File to upload";
            // 
            // txtFileUploadPath
            // 
            this.txtFileUploadPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFileUploadPath.Location = new System.Drawing.Point(73, 3);
            this.txtFileUploadPath.Name = "txtFileUploadPath";
            this.txtFileUploadPath.Properties.ReadOnly = true;
            this.txtFileUploadPath.Size = new System.Drawing.Size(525, 20);
            this.txtFileUploadPath.TabIndex = 1;
            // 
            // btnSetFilePath
            // 
            this.btnSetFilePath.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSetFilePath.Appearance.Options.UseFont = true;
            this.btnSetFilePath.Location = new System.Drawing.Point(604, 3);
            this.btnSetFilePath.Name = "btnSetFilePath";
            this.btnSetFilePath.Size = new System.Drawing.Size(29, 20);
            this.btnSetFilePath.TabIndex = 2;
            this.btnSetFilePath.Text = "...";
            this.btnSetFilePath.Click += new System.EventHandler(this.btnSetFilePath_Click);
            // 
            // btnUploadFile
            // 
            this.btnUploadFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUploadFile.Location = new System.Drawing.Point(481, 3);
            this.btnUploadFile.Name = "btnUploadFile";
            this.btnUploadFile.Size = new System.Drawing.Size(75, 23);
            this.btnUploadFile.TabIndex = 7;
            this.btnUploadFile.Text = "Save";
            this.btnUploadFile.Click += new System.EventHandler(this.btnUploadFile_Click);
            // 
            // GroupControl2
            // 
            this.GroupControl2.Controls.Add(this.TableLayoutPanel4);
            this.GroupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupControl2.Location = new System.Drawing.Point(3, 60);
            this.GroupControl2.Name = "GroupControl2";
            this.GroupControl2.Size = new System.Drawing.Size(640, 397);
            this.GroupControl2.TabIndex = 1;
            this.GroupControl2.Text = "Messages";
            // 
            // TableLayoutPanel4
            // 
            this.TableLayoutPanel4.ColumnCount = 1;
            this.TableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel4.Controls.Add(this.XtraTabControl1, 0, 0);
            this.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel4.Location = new System.Drawing.Point(2, 23);
            this.TableLayoutPanel4.Name = "TableLayoutPanel4";
            this.TableLayoutPanel4.RowCount = 1;
            this.TableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel4.Size = new System.Drawing.Size(636, 372);
            this.TableLayoutPanel4.TabIndex = 2;
            // 
            // XtraTabControl1
            // 
            this.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.XtraTabControl1.Location = new System.Drawing.Point(3, 3);
            this.XtraTabControl1.Name = "XtraTabControl1";
            this.XtraTabControl1.SelectedTabPage = this.XtraTabPageGeneralInfo;
            this.XtraTabControl1.Size = new System.Drawing.Size(630, 366);
            this.XtraTabControl1.TabIndex = 2;
            this.XtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.XtraTabPageGeneralInfo});
            // 
            // XtraTabPageGeneralInfo
            // 
            this.XtraTabPageGeneralInfo.Controls.Add(this.MemoEditFileFormat);
            this.XtraTabPageGeneralInfo.Name = "XtraTabPageGeneralInfo";
            this.XtraTabPageGeneralInfo.Size = new System.Drawing.Size(628, 343);
            this.XtraTabPageGeneralInfo.Text = "General Info";
            // 
            // MemoEditFileFormat
            // 
            this.MemoEditFileFormat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MemoEditFileFormat.Location = new System.Drawing.Point(0, 0);
            this.MemoEditFileFormat.Name = "MemoEditFileFormat";
            this.MemoEditFileFormat.Size = new System.Drawing.Size(628, 343);
            this.MemoEditFileFormat.TabIndex = 0;
            // 
            // TableLayoutPanel3
            // 
            this.TableLayoutPanel3.AutoSize = true;
            this.TableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TableLayoutPanel3.ColumnCount = 4;
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.Controls.Add(this.btnCancel, 3, 0);
            this.TableLayoutPanel3.Controls.Add(this.btnUploadFile, 2, 0);
            this.TableLayoutPanel3.Controls.Add(this.CheckEditUpdateDatabase, 1, 0);
            this.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.TableLayoutPanel3.Location = new System.Drawing.Point(3, 463);
            this.TableLayoutPanel3.Name = "TableLayoutPanel3";
            this.TableLayoutPanel3.RowCount = 1;
            this.TableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel3.Size = new System.Drawing.Size(640, 29);
            this.TableLayoutPanel3.TabIndex = 2;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(562, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // CheckEditUpdateDatabase
            // 
            this.CheckEditUpdateDatabase.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.CheckEditUpdateDatabase.Location = new System.Drawing.Point(351, 5);
            this.CheckEditUpdateDatabase.Name = "CheckEditUpdateDatabase";
            this.CheckEditUpdateDatabase.Properties.Caption = "Update database";
            this.CheckEditUpdateDatabase.Properties.ReadOnly = true;
            this.CheckEditUpdateDatabase.Size = new System.Drawing.Size(124, 18);
            this.CheckEditUpdateDatabase.TabIndex = 3;
            // 
            // FormGEWUpload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(646, 495);
            this.Controls.Add(this.TableLayoutPanel2);
            this.Name = "FormGEWUpload";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Upload GEW File";
            this.Load += new System.EventHandler(this.FormGEWUpload_Load);
            this.TableLayoutPanel2.ResumeLayout(false);
            this.TableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).EndInit();
            this.GroupControl1.ResumeLayout(false);
            this.GroupControl1.PerformLayout();
            this.TableLayoutPanel1.ResumeLayout(false);
            this.TableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFileUploadPath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).EndInit();
            this.GroupControl2.ResumeLayout(false);
            this.TableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.XtraTabControl1)).EndInit();
            this.XtraTabControl1.ResumeLayout(false);
            this.XtraTabPageGeneralInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MemoEditFileFormat.Properties)).EndInit();
            this.TableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditUpdateDatabase.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel2;
        internal DevExpress.XtraEditors.GroupControl GroupControl1;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.TextEdit txtFileUploadPath;
        internal DevExpress.XtraEditors.SimpleButton btnSetFilePath;
        internal DevExpress.XtraEditors.SimpleButton btnUploadFile;
        internal DevExpress.XtraEditors.GroupControl GroupControl2;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel4;
        internal DevExpress.XtraTab.XtraTabControl XtraTabControl1;
        internal DevExpress.XtraTab.XtraTabPage XtraTabPageGeneralInfo;
        internal DevExpress.XtraEditors.MemoEdit MemoEditFileFormat;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel3;
        internal DevExpress.XtraEditors.SimpleButton btnCancel;
        internal DevExpress.XtraEditors.CheckEdit CheckEditUpdateDatabase;
    }
}