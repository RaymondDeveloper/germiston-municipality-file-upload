﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EKU.DAL;
using EKU.Globals;

namespace EKU.GEW
{
    public partial class FormViewUploads : DevExpress.XtraEditors.XtraForm
    {
        private int _importId = 0;
        private DAL.EKUDataSetTableAdapters.ImportsTableAdapter taImports = new DAL.EKUDataSetTableAdapters.ImportsTableAdapter();

        public FormViewUploads()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormViewUploads_Load(object sender, EventArgs e)
        {
            DateTime currentdate = DateTime.Today;

            DateTime startdate = currentdate.AddDays(-30);
            DateEditPeriodStartDate.DateTime = startdate;
            DateEditPeriodEndDate.DateTime = currentdate;

            Result results = LoadData();

            if (results.resultCode != 0)
            {
                MessageBox.Show(results.resultMessage, "Imports", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private Result LoadData()
        {
            DateTime? startDate = null;
            DateTime? endDate = null;

            List<string> errors = new List<string>();

            if (CheckEditFilterByPeriod.Checked == true)
            {
                if (DateEditPeriodStartDate.DateTime > new DateTime(1900, 1, 1))
                    startDate = DateEditPeriodStartDate.DateTime;
                else
                    errors.Add("Invalid start date specified");

                if (DateEditPeriodEndDate.DateTime > new DateTime(1900, 1, 1))
                    endDate = DateEditPeriodEndDate.DateTime;
                else
                    errors.Add("Invalid end date specified");

                if (startDate.HasValue & endDate.HasValue)
                {
                    if (startDate.Value <= endDate.Value)
                    {
                    }
                    else
                        errors.Add("The period start date must be on or before the end date");
                }
            }

            if (errors.Count == 0)
            {
                try
                {
                    if (CheckEditFilterByPeriod.Checked == true)
                        this.virtualImportsTableAdapter1.FillByRunDatePeriod(this.eKUDataSet1.virtualImports, startDate.Value, endDate.Value.AddDays(1).AddTicks(-1));
                    else
                        this.virtualImportsTableAdapter1.Fill(this.eKUDataSet1.virtualImports);

                    return CodeExecution.Succeeded();
                }
                catch (Exception ex)
                {
                    return CodeExecution.Failed(string.Format("Failed to load imports: {0}", ex.Message));
                }
            }
            else
            {
                string combinedstring = string.Concat("Please correct the following errors before continuing: ", Environment.NewLine);

                foreach (var errorfound in errors)
                {
                    combinedstring = string.Concat(combinedstring, Environment.NewLine, errorfound);
                }

                return CodeExecution.Failed(combinedstring);
            }
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            int currentRowHandle = GridViewImports.GetVisibleRowHandle(0);
            while (currentRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                EKUDataSet.virtualImportsRow dr =
                    (EKUDataSet.virtualImportsRow)GridViewImports.GetDataRow(currentRowHandle);
                dr.IsSelected = true;
                dr.AcceptChanges();

                currentRowHandle = GridViewImports.GetNextVisibleRow(currentRowHandle);
            }
        }

        private void btnUnselectAll_Click(object sender, EventArgs e)
        {
            int currentRowHandle = GridViewImports.GetVisibleRowHandle(0);
            while (currentRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                EKUDataSet.virtualImportsRow dr =
                    (EKUDataSet.virtualImportsRow)GridViewImports.GetDataRow(currentRowHandle);
                dr.IsSelected = false;
                dr.AcceptChanges();

                currentRowHandle = GridViewImports.GetNextVisibleRow(currentRowHandle);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Result results = LoadData();

            if (results.resultCode != 0)
            {
                MessageBox.Show(results.resultMessage, "Imports", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (_importId > 0)
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete the selected import?",
                        "Confirm import deletion", MessageBoxButtons.YesNo);

                    if (dialogResult == DialogResult.Yes)
                    {
                        taImports.DeleteImport(_importId);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Failed to delete import: {0}", ex.Message), "Imports", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReprocessImports_Click(object sender, EventArgs e)
        {

        }

        //private void GridViewImports_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        //{
        //    try
        //    {
        //        EKUDataSet.virtualImportsRow dr = (EKUDataSet.virtualImportsRow)GridViewImports.GetFocusedDataRow();
        //        _importId = dr.pkImportID;
        //    }
        //    catch (Exception)
        //    {
        //        _importId = 0;
        //    }
        //}

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            List<int> selectedimports = this.eKUDataSet1.virtualImports
                .Where(p => p.IsSelected == true)
                    .Select(p => p.pkImportID).ToList();

            if (selectedimports.Any())
            {
                DialogResult dialogResult = MessageBox.Show(string.Format("Are you sure you want to delete ", selectedimports.Count().ToString(), " selected imports?"),
                    "Delete Imports", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    foreach (int importID in selectedimports)
                        taImports.DeleteImport(importID);

                    LoadData();
                }
            }
        }
    }
}