﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EKU.Globals;

namespace EKU.GEW
{
    public partial class FormGEWUpload : DevExpress.XtraEditors.XtraForm
    {
        UploadGEW _upload = new UploadGEW();
        private string fileToUpload = "";

        public FormGEWUpload()
        {
            InitializeComponent();
        }

        private void FormGEWUpload_Load(object sender, EventArgs e)
        {
            MemoEditFileFormat.Text = _upload.FileFormat;
        }

        private void btnSetFilePath_Click(object sender, EventArgs e)
        {
            string FilePath;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                FilePath = openFileDialog1.FileName;
                fileToUpload = FilePath;
                txtFileUploadPath.Text = FilePath;
            }
        }

        private void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFileUploadPath.Text))
            {
                MessageBox.Show("Please select a file to upload.", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                progressWindowUpload progress = new progressWindowUpload(txtFileUploadPath.Text, CheckEditUpdateDatabase.Checked);
                progress.ShowDialog();

                Result results;
                results = progress.searchResult;

                if (results.resultCode == 0)
                {
                    MessageBox.Show("Successfully generated Mobiz file", "Mobiz", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(results.resultMessage, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}