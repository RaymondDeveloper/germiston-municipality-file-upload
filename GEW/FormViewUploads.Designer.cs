﻿namespace EKU.GEW
{
    partial class FormViewUploads
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.virtualImportsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GridViewImports = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIsSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colpkImportID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRunDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEffectiveDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecords = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFilePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImportSuccessful = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEditFilterByPeriod = new DevExpress.XtraEditors.CheckEdit();
            this.DateEditPeriodStartDate = new DevExpress.XtraEditors.DateEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.DateEditPeriodEndDate = new DevExpress.XtraEditors.DateEdit();
            this.TableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnReprocessImports = new DevExpress.XtraEditors.SimpleButton();
            this.btnSelectAll = new DevExpress.XtraEditors.SimpleButton();
            this.btnUnselectAll = new DevExpress.XtraEditors.SimpleButton();
            this.TableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.virtualImportsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewImports)).BeginInit();
            this.TableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditFilterByPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodEndDate.Properties)).BeginInit();
            this.TableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.ColumnCount = 1;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.Controls.Add(this.GridControl1, 0, 1);
            this.TableLayoutPanel1.Controls.Add(this.TableLayoutPanel2, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.TableLayoutPanel3, 0, 2);
            this.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 3;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(921, 524);
            this.TableLayoutPanel1.TabIndex = 1;
            // 
            // GridControl1
            // 
            this.GridControl1.DataSource = this.virtualImportsBindingSource;
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridControl1.Location = new System.Drawing.Point(3, 35);
            this.GridControl1.MainView = this.GridViewImports;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(915, 451);
            this.GridControl1.TabIndex = 3;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridViewImports});
            // 
            // GridViewImports
            // 
            this.GridViewImports.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.GridViewImports.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridViewImports.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GridViewImports.ColumnPanelRowHeight = 40;
            this.GridViewImports.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIsSelected,
            this.colpkImportID,
            this.colRunDate,
            this.colFileName,
            this.colEffectiveDate,
            this.colRecords,
            this.colFilePath,
            this.colImportSuccessful});
            this.GridViewImports.GridControl = this.GridControl1;
            this.GridViewImports.Name = "GridViewImports";
            this.GridViewImports.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GridViewImports.OptionsSelection.MultiSelect = true;
            this.GridViewImports.OptionsView.ShowAutoFilterRow = true;
            this.GridViewImports.OptionsView.ShowDetailButtons = false;
            this.GridViewImports.OptionsView.ShowGroupPanel = false;
            this.GridViewImports.OptionsView.ShowIndicator = false;
            // 
            // colIsSelected
            // 
            this.colIsSelected.FieldName = "IsSelected";
            this.colIsSelected.Name = "colIsSelected";
            this.colIsSelected.OptionsColumn.ShowCaption = false;
            this.colIsSelected.Visible = true;
            this.colIsSelected.VisibleIndex = 0;
            this.colIsSelected.Width = 20;
            // 
            // colpkImportID
            // 
            this.colpkImportID.FieldName = "pkImportID";
            this.colpkImportID.Name = "colpkImportID";
            this.colpkImportID.OptionsColumn.AllowEdit = false;
            this.colpkImportID.OptionsColumn.ReadOnly = true;
            this.colpkImportID.Width = 127;
            // 
            // colRunDate
            // 
            this.colRunDate.FieldName = "RunDate";
            this.colRunDate.Name = "colRunDate";
            this.colRunDate.OptionsColumn.AllowEdit = false;
            this.colRunDate.OptionsColumn.ReadOnly = true;
            this.colRunDate.Visible = true;
            this.colRunDate.VisibleIndex = 1;
            this.colRunDate.Width = 127;
            // 
            // colFileName
            // 
            this.colFileName.FieldName = "FileName";
            this.colFileName.Name = "colFileName";
            this.colFileName.OptionsColumn.AllowEdit = false;
            this.colFileName.OptionsColumn.ReadOnly = true;
            this.colFileName.Visible = true;
            this.colFileName.VisibleIndex = 2;
            this.colFileName.Width = 127;
            // 
            // colEffectiveDate
            // 
            this.colEffectiveDate.FieldName = "EffectiveDate";
            this.colEffectiveDate.Name = "colEffectiveDate";
            this.colEffectiveDate.OptionsColumn.AllowEdit = false;
            this.colEffectiveDate.OptionsColumn.ReadOnly = true;
            this.colEffectiveDate.Visible = true;
            this.colEffectiveDate.VisibleIndex = 3;
            this.colEffectiveDate.Width = 127;
            // 
            // colRecords
            // 
            this.colRecords.FieldName = "Records";
            this.colRecords.Name = "colRecords";
            this.colRecords.OptionsColumn.AllowEdit = false;
            this.colRecords.OptionsColumn.ReadOnly = true;
            this.colRecords.Visible = true;
            this.colRecords.VisibleIndex = 4;
            this.colRecords.Width = 127;
            // 
            // colFilePath
            // 
            this.colFilePath.FieldName = "FilePath";
            this.colFilePath.Name = "colFilePath";
            this.colFilePath.OptionsColumn.AllowEdit = false;
            this.colFilePath.OptionsColumn.ReadOnly = true;
            this.colFilePath.Visible = true;
            this.colFilePath.VisibleIndex = 5;
            this.colFilePath.Width = 127;
            // 
            // colImportSuccessful
            // 
            this.colImportSuccessful.FieldName = "ImportSuccessful";
            this.colImportSuccessful.Name = "colImportSuccessful";
            this.colImportSuccessful.OptionsColumn.AllowEdit = false;
            this.colImportSuccessful.OptionsColumn.ReadOnly = true;
            this.colImportSuccessful.Visible = true;
            this.colImportSuccessful.VisibleIndex = 6;
            this.colImportSuccessful.Width = 131;
            // 
            // TableLayoutPanel2
            // 
            this.TableLayoutPanel2.AutoSize = true;
            this.TableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TableLayoutPanel2.ColumnCount = 6;
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel2.Controls.Add(this.btnRefresh, 5, 0);
            this.TableLayoutPanel2.Controls.Add(this.CheckEditFilterByPeriod, 1, 0);
            this.TableLayoutPanel2.Controls.Add(this.DateEditPeriodStartDate, 2, 0);
            this.TableLayoutPanel2.Controls.Add(this.LabelControl1, 3, 0);
            this.TableLayoutPanel2.Controls.Add(this.DateEditPeriodEndDate, 4, 0);
            this.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.TableLayoutPanel2.Name = "TableLayoutPanel2";
            this.TableLayoutPanel2.RowCount = 1;
            this.TableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel2.Size = new System.Drawing.Size(915, 26);
            this.TableLayoutPanel2.TabIndex = 0;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(837, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 20);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // CheckEditFilterByPeriod
            // 
            this.CheckEditFilterByPeriod.EditValue = true;
            this.CheckEditFilterByPeriod.Location = new System.Drawing.Point(459, 3);
            this.CheckEditFilterByPeriod.Name = "CheckEditFilterByPeriod";
            this.CheckEditFilterByPeriod.Properties.Caption = "Filter by run date";
            this.CheckEditFilterByPeriod.Size = new System.Drawing.Size(144, 18);
            this.CheckEditFilterByPeriod.TabIndex = 0;
            // 
            // DateEditPeriodStartDate
            // 
            this.DateEditPeriodStartDate.EditValue = null;
            this.DateEditPeriodStartDate.Location = new System.Drawing.Point(609, 3);
            this.DateEditPeriodStartDate.Name = "DateEditPeriodStartDate";
            this.DateEditPeriodStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEditPeriodStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEditPeriodStartDate.Size = new System.Drawing.Size(100, 20);
            this.DateEditPeriodStartDate.TabIndex = 1;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelControl1.Location = new System.Drawing.Point(715, 6);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(10, 13);
            this.LabelControl1.TabIndex = 3;
            this.LabelControl1.Text = "to";
            // 
            // DateEditPeriodEndDate
            // 
            this.DateEditPeriodEndDate.EditValue = null;
            this.DateEditPeriodEndDate.Location = new System.Drawing.Point(731, 3);
            this.DateEditPeriodEndDate.Name = "DateEditPeriodEndDate";
            this.DateEditPeriodEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEditPeriodEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEditPeriodEndDate.Size = new System.Drawing.Size(100, 20);
            this.DateEditPeriodEndDate.TabIndex = 2;
            // 
            // TableLayoutPanel3
            // 
            this.TableLayoutPanel3.AutoSize = true;
            this.TableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TableLayoutPanel3.ColumnCount = 7;
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanel3.Controls.Add(this.btnDelete, 5, 0);
            this.TableLayoutPanel3.Controls.Add(this.btnClose, 6, 0);
            this.TableLayoutPanel3.Controls.Add(this.btnSelectAll, 0, 0);
            this.TableLayoutPanel3.Controls.Add(this.btnUnselectAll, 1, 0);
            this.TableLayoutPanel3.Controls.Add(this.btnReprocessImports, 4, 0);
            this.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel3.Location = new System.Drawing.Point(3, 492);
            this.TableLayoutPanel3.Name = "TableLayoutPanel3";
            this.TableLayoutPanel3.RowCount = 1;
            this.TableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel3.Size = new System.Drawing.Size(915, 29);
            this.TableLayoutPanel3.TabIndex = 1;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(756, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click_1);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(837, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnReprocessImports
            // 
            this.btnReprocessImports.Location = new System.Drawing.Point(675, 3);
            this.btnReprocessImports.Name = "btnReprocessImports";
            this.btnReprocessImports.Size = new System.Drawing.Size(75, 23);
            this.btnReprocessImports.TabIndex = 6;
            this.btnReprocessImports.Text = "Export Mobiz";
            this.btnReprocessImports.Click += new System.EventHandler(this.btnReprocessImports_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(3, 3);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(75, 23);
            this.btnSelectAll.TabIndex = 4;
            this.btnSelectAll.Text = "Select all";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnUnselectAll
            // 
            this.btnUnselectAll.Location = new System.Drawing.Point(84, 3);
            this.btnUnselectAll.Name = "btnUnselectAll";
            this.btnUnselectAll.Size = new System.Drawing.Size(75, 23);
            this.btnUnselectAll.TabIndex = 5;
            this.btnUnselectAll.Text = "Unselect all";
            this.btnUnselectAll.Click += new System.EventHandler(this.btnUnselectAll_Click);
            // 
            // FormViewUploads
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 524);
            this.Controls.Add(this.TableLayoutPanel1);
            this.Name = "FormViewUploads";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Uploads";
            this.Load += new System.EventHandler(this.FormViewUploads_Load);
            this.TableLayoutPanel1.ResumeLayout(false);
            this.TableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.virtualImportsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewImports)).EndInit();
            this.TableLayoutPanel2.ResumeLayout(false);
            this.TableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditFilterByPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditPeriodEndDate.Properties)).EndInit();
            this.TableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel2;
        internal DevExpress.XtraEditors.SimpleButton btnRefresh;
        internal DevExpress.XtraEditors.CheckEdit CheckEditFilterByPeriod;
        internal DevExpress.XtraEditors.DateEdit DateEditPeriodStartDate;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.DateEdit DateEditPeriodEndDate;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel3;
        internal DevExpress.XtraEditors.SimpleButton btnClose;
        internal DevExpress.XtraEditors.SimpleButton btnReprocessImports;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridViewImports;
        private DAL.EKUDataSet eKUDataSet;
        private DAL.EKUDataSetTableAdapters.virtualImportsTableAdapter virtualImportsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colpkImportID;
        private DevExpress.XtraGrid.Columns.GridColumn colRunDate;
        private DevExpress.XtraGrid.Columns.GridColumn colFileName;
        private DevExpress.XtraGrid.Columns.GridColumn colEffectiveDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRecords;
        private DevExpress.XtraGrid.Columns.GridColumn colFilePath;
        private DevExpress.XtraGrid.Columns.GridColumn colImportSuccessful;
        private DAL.EKUDataSet eKUDataSet1;
        private System.Windows.Forms.BindingSource virtualImportsBindingSource;
        private DAL.EKUDataSetTableAdapters.virtualImportsTableAdapter virtualImportsTableAdapter1;
        internal DevExpress.XtraEditors.SimpleButton btnDelete;
        internal DevExpress.XtraEditors.SimpleButton btnSelectAll;
        internal DevExpress.XtraEditors.SimpleButton btnUnselectAll;

    }
}