﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EKU.Globals;

namespace EKU.GEW
{
    public partial class FormViewAccounts : DevExpress.XtraEditors.XtraForm
    {
        public FormViewAccounts()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormViewAccounts_Load(object sender, EventArgs e)
        {
            DateTime currentdate = DateTime.Today;

            DateTime startdate = currentdate.AddDays(-30);
            DateEditPeriodStartDate.DateTime = startdate;
            DateEditPeriodEndDate.DateTime = currentdate;

            Result results = LoadData();

            if (results.resultCode != 0)
            {
                MessageBox.Show(results.resultMessage, "Imports", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Result results = LoadData();

            if (results.resultCode != 0)
            {
                MessageBox.Show(results.resultMessage, "Imports", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private Result LoadData()
        {
            DateTime? startDate = null;
            DateTime? endDate = null;

            List<string> errors = new List<string>();

            if (CheckEditFilterByPeriod.Checked == true)
            {
                if (DateEditPeriodStartDate.DateTime > new DateTime(1900, 1, 1))
                    startDate = DateEditPeriodStartDate.DateTime;
                else
                    errors.Add("Invalid start date specified");

                if (DateEditPeriodEndDate.DateTime > new DateTime(1900, 1, 1))
                    endDate = DateEditPeriodEndDate.DateTime;
                else
                    errors.Add("Invalid end date specified");

                if (startDate.HasValue & endDate.HasValue)
                {
                    if (startDate.Value <= endDate.Value)
                    {
                    }
                    else
                        errors.Add("The period start date must be on or before the end date");
                }
            }

            if (errors.Count == 0)
            {
                try
                {
                    if (CheckEditFilterByPeriod.Checked == true)
                        this.virtualAccountsTableAdapter.FillByRunDatePeriod(this.eKUDataSet.virtualAccounts, startDate.Value, endDate.Value.AddDays(1).AddTicks(-1));
                    else
                        this.virtualAccountsTableAdapter.Fill(this.eKUDataSet.virtualAccounts);

                    return CodeExecution.Succeeded();
                }
                catch (Exception ex)
                {
                    return CodeExecution.Failed(string.Format("Failed to load imports: {0}", ex.Message));
                }
            }
            else
            {
                string combinedstring = string.Concat("Please correct the following errors before continuing: ", Environment.NewLine);

                foreach (var errorfound in errors)
                {
                    combinedstring = string.Concat(combinedstring, Environment.NewLine, errorfound);
                }

                return CodeExecution.Failed(combinedstring);
            }
        }
    }
}