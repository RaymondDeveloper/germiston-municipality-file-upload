﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EKU.Globals;

namespace EKU.GEW
{
    public partial class progressWindowUpload : DevExpress.XtraEditors.XtraForm
    {
        private BackgroundWorker backgroundProgress = new BackgroundWorker();

        private Result _searchResult;

        private string _path;

        private bool _updateDatabase;

        public Result searchResult
        {
            get { return _searchResult; }
        }

        public progressWindowUpload(string path, bool updateDatabase)
        {
            InitializeComponent();
            backgroundProgress.DoWork += backgroundWorker1_DoWork;
            backgroundProgress.RunWorkerCompleted += backgroundWorker1_RunWorkerCompleted;
            _path = path;
            _updateDatabase = updateDatabase;
        }

        private void progressWindowUpload_Load(object sender, EventArgs e)
        {
            backgroundProgress.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            UploadGEW up = new UploadGEW(_path, _updateDatabase);
            e.Result = up.Upload();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Error != null))
            {
                _searchResult.resultCode = 1;
                _searchResult.resultMessage = e.Error.Message;
                this.Close();
            }
            else if (e.Cancelled)
            {
                _searchResult.resultCode = 1;
                _searchResult.resultMessage = "The search was cancelled.";
                this.Close();
            }
            else
            {
                _searchResult = (Result)e.Result;
                this.Close();
            }
        }
    }
}